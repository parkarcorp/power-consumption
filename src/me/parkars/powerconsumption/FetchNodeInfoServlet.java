package me.parkars.powerconsumption;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import me.parkars.powerconsumption.model.NodeInfo;
import me.parkars.powerconsumption.model.PMFHttpServlet;

@SuppressWarnings("serial")
public class FetchNodeInfoServlet extends PMFHttpServlet {
	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		List<NodeInfo> nodeInfo = getPersistentObjects(NodeInfo.class);
		String response;
		if(nodeInfo == null || nodeInfo.isEmpty()) {
			response = "No Data";
		} else {
			response = new Gson().toJson(nodeInfo);
		}
		
		
		resp.getWriter().write(response);
	}
}
