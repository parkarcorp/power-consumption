package me.parkars.powerconsumption;

import java.io.IOException;

import javax.jdo.PersistenceManager;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import me.parkars.powerconsumption.model.Node2;
import me.parkars.powerconsumption.model.NodeInfo;
import me.parkars.powerconsumption.model.PMFHttpServlet;
import me.parkars.powerconsumption.model.ServerResponse;

@SuppressWarnings("serial")
public class UpdateNode2Servlet extends PMFHttpServlet {

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String jsonRequest = getRequestString(req);

		ServerResponse response;

		if (!StringUtils.isNullOrEmpty(jsonRequest)) {
			Node2 nodeData = new Gson().fromJson(jsonRequest, Node2.class);
			Node2 currentData = getPersistentObject(Node2.class, "Fan");

			if (StringUtils.isNullOrEmpty(nodeData.getStatus())) {
				if (currentData != null) {
					nodeData.setStatus(currentData.getStatus());
				} else {
					nodeData.setStatus("off");
				}
			}

			if (StringUtils.isNullOrEmpty(nodeData.getSpeed())) {
				if (currentData != null) {
					nodeData.setSpeed(currentData.getSpeed());
				} else {
					nodeData.setSpeed("1");
				}
			}

			if (StringUtils.isNullOrEmpty(nodeData.getCurrent())) {
				if (currentData != null) {
					nodeData.setCurrent(currentData.getCurrent());
				} else {
					nodeData.setCurrent("0");
				}
			}

			NodeInfo info = new NodeInfo(nodeData.getName(), nodeData.getCurrent());

			if (doPersistent(nodeData, info)) {
				response = new ServerResponse(0, null, nodeData);
			} else {
				response = new ServerResponse(1, "Failed to update value", null);
			}
		} else {
			response = new ServerResponse(2, "Invalid Request", null);
		}

		resp.getWriter().write(new Gson().toJson(response));

	}

	protected boolean doPersistent(Node2 info, NodeInfo log) {
		PersistenceManager persistenceManager = getPMF().getPersistenceManager();

		try {
			persistenceManager.makePersistent(log);
			persistenceManager.makePersistent(info);
		} catch (Exception e) {
			return false;
		} finally {
			persistenceManager.close();
		}

		return true;
	}
}
