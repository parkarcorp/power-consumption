package me.parkars.powerconsumption;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import me.parkars.powerconsumption.model.Node1;
import me.parkars.powerconsumption.model.Node2;
import me.parkars.powerconsumption.model.PMFHttpServlet;

public class FetchNodeDataServlet extends PMFHttpServlet{

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		Node1 node1 = getPersistentObject(Node1.class, "Bulb");
		Node2 node2 = getPersistentObject(Node2.class, "Fan");
		
		List<Object> data = new ArrayList<Object>(2);
		data.add(node1);
		data.add(node2);
		
		resp.getWriter().write(new Gson().toJson(data));
	}
}
