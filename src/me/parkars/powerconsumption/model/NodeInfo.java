package me.parkars.powerconsumption.model;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.TimeZone;

import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;

@PersistenceCapable
public class NodeInfo {
	
	@Persistent
	String nodeName;
	
	@Persistent
	String current;
	
	@Persistent
	String timestamp;
	
	

	public NodeInfo(String nodeName, String current) {
		this.nodeName = nodeName;
		this.current = current;
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
        dateFormat.setTimeZone(TimeZone.getTimeZone("IST"));

        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("IST"));
        this.timestamp = dateFormat.format(cal.getTime());
	}

	public String getNodeName() {
		return nodeName;
	}

	public void setNodeName(String nodeName) {
		this.nodeName = nodeName;
	}

	public String getCurrent() {
		return current;
	}

	public void setCurrent(String current) {
		this.current = current;
	}

	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	
	
}
