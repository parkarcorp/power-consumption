package me.parkars.powerconsumption.model;

import com.google.gson.annotations.Expose;

/**
 * @author basitparkar
 */
public class ServerResponse {

	@Expose
    int errorCode;
	
	@Expose
    String errorDesc;
	
	@Expose
    Object data;

    public ServerResponse(int errorCode, String errorDesc, Object data) {
        this.errorCode = errorCode;
        this.errorDesc = errorDesc;
        this.data = data;
    }

	public int getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorDesc() {
		return errorDesc;
	}

	public void setErrorDesc(String errorDesc) {
		this.errorDesc = errorDesc;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}
    
    
}
